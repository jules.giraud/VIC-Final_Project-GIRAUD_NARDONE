import numpy as np
from skimage import exposure
import rasterio
import cv2

def import_images(sar_path:str, mask_path:str, hydro_path=None):
    """Import images using rasterio library

    Args:
        sar_path (str): Path to the SAR image
        mask_path (str): Path to the MASK image

    Returns:
        tuple: Tuple of np.arrays
    """
    with rasterio.open(str(sar_path), "r", driver="GTiff") as src:
            image = src.read().transpose(1, 2, 0)
            # vv = image[:, :, 0] We only use VH band
            vh = image[:, :, 1]

    with rasterio.open(str(mask_path), "r", driver="GTiff") as src:
            image = src.read().transpose(1, 2, 0)
            mask = np.abs(image[:, :, 0])

    if hydro_path:
        with rasterio.open(str(hydro_path), "r", driver="GTiff") as src:
            image = src.read().transpose(1, 2, 0)
            hydro = image[:, :, 0]
        return vh, mask, hydro
        
    return vh, mask

def preprocess(sar_img, stretching=True, equalizer=True):
    """Preprocess the images using differents methods

    Args:
        sar_img (np.array): Image
        stretching (bool, optional): Apply a histogram stretching. Defaults to True.
        equalizer (bool, optional): Apply an histogram equalizer. Defaults to True.

    Returns:
        np.array: Image preprocessed
    """
    sar_img_preproc = 10 * np.log10(sar_img + 0.01)
    if(stretching):
        p2, p98 = np.percentile(sar_img_preproc, (2, 98))
        sar_img_preproc = exposure.rescale_intensity(sar_img_preproc, in_range=(p2, p98))
    if(equalizer):
        sar_img_preproc = exposure.equalize_hist(sar_img)
    return sar_img_preproc

def normalize(sar_img):
    normalized_img = cv2.normalize(sar_img, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    normalized_img = normalized_img.astype(np.uint8)
    return normalized_img

def lee_filter(img, window_size, sigma):
    # Pad the image with zeros
    img_padded = np.pad(img, window_size, mode='constant')
    img_filtered = np.zeros(img.shape)
    # Iterate over each pixel in the image
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            # Extract the window around the current pixel
            window = img_padded[i:i+window_size*2+1, j:j+window_size*2+1]
            # Compute the mean and standard deviation of the window
            mean = np.mean(window)
            std = np.std(window)
            # Compute the filtered value for the current pixel
            img_filtered[i, j] = mean + sigma * std
    return img_filtered
