import cv2
import numpy as np
import matplotlib.pyplot as plt

def apply_threshold(threshold_method:str, image, threshold_value=120):
    """Apply threshold method

    Args:
        threshold_method (str): ('binary', 'trunc', 'tozero', 'adp_mean', 'adp_gauss')
        image (np.array): Image
        threshold_value (int): Threshold value between 0 and 255

    Raises:
        TypeError: If the threshold method is not a string
        ValueError: If one of the threshold methods doesn't exist

    Returns:
        pred: Mask image as an np.array
    """
    if not isinstance(threshold_method, str):
        raise TypeError("Threshold method should be a string.")

    if threshold_method == 'binary':
        _, pred = cv2.threshold(image, threshold_value, 255, cv2.THRESH_BINARY)
    elif threshold_method == 'trunc':
        _, pred = cv2.threshold(image, threshold_value, 255, cv2.THRESH_TRUNC)
    elif threshold_method == 'tozero':
        _, pred = cv2.threshold(image, threshold_value, 255, cv2.THRESH_TOZERO)
    elif threshold_method == 'adp_mean':
        pred = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 7, 1)
    elif threshold_method == 'adp_gauss':
        pred = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    else:
        raise ValueError(f'Invalid threshold method: {threshold_method}')
    return pred

def all_thresholds(image, threshold_value):
    """Apply all the threshold methods

    Args:
        image (np.array): Image
        threshold_value (int): Threshold value between 0 and 255

    Returns:
        list: List of all the mask images corresponding to the predictions
    """
    threshold_methods = ['binary', 'trunc', 'tozero', 'adp_mean', 'adp_gauss']
    return [apply_threshold(method, image, threshold_value) for method in threshold_methods]

def apply_otsu_threshold(threshold_method:str, image, kernel_size=(3, 3)):
    """Apply otsu threshold method

    Args:
        threshold_method (str): ('otsu', 'gauss_otsu')
        image (np.array): Image
        kernel_size (tuple, optional): Size of the kernel applied on the Gaussian filter. Defaults to (3, 3).


    Raises:
        TypeError: If the threshold method is not a string
        ValueError: If one of the threshold methods doesn't exist

    Returns:
        pred: Mask image as an np.array
    """
    if not isinstance(threshold_method, str):
        raise TypeError("Threshold method should be a string.")

    if threshold_method == 'otsu':
        _, pred = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    elif threshold_method == 'gauss_otsu':
        blur_noise_img = cv2.GaussianBlur(image, ksize=kernel_size, sigmaX=0)
        _, pred = cv2.threshold(blur_noise_img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    else:
        raise ValueError(f'Invalid threshold method: {threshold_method}')
    return pred

def all_otsu_thresholds(image, kernel_size=(3, 3)):
    """Apply all the adptative threshold methods

    Args:
        image (np.array): Image

    Returns:
        list: List of all the mask images corresponding to the predictions
    """
    threshold_methods = ['otsu', 'gauss_otsu']
    return [apply_otsu_threshold(method, image, kernel_size) for method in threshold_methods]

def noise_filtering(filter_method, image, kernel_size=(3, 3), iterations=1):
    """Apply morphological filters on the images to remove small and isolated polygons

    Args:
        filter_method (str): ('dilatation', 'erosion', 'opening', 'closing', 'gradient', 'open_n_close')
        image (np.array): _description_
        kernel_size (tuple, optional): Size of the kernel applied on each filter. Defaults to (3, 3).
        iterations (int, optional): Number of iteration of the filter on the image. Defaults to 1.

    Raises:
        TypeError: _description_
        ValueError: _description_

    Returns:
        _type_: _description_
    """
    if not isinstance(filter_method, str):
        raise TypeError("Filtering method should be a string.")

    kernel = np.ones(kernel_size, np.uint8)

    if filter_method == 'dilatation':
        filtered_img = cv2.dilate(image, kernel, iterations)
    elif filter_method == 'erosion':
        filtered_img = cv2.erode(image, kernel, iterations)
    elif filter_method == 'opening':
        filtered_img = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
    elif filter_method == 'closing':
        filtered_img = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    elif filter_method == 'gradient':
        filtered_img = cv2.morphologyEx(image, cv2.MORPH_GRADIENT, kernel)
    elif filter_method == 'open_n_close':
        opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
        filtered_img = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    else:
        raise ValueError(f'Invalid threshold method: {filter_method}')
    return filtered_img


def display_all_noised_images(image, mask, kernel_size=(3,3), iterations=1):
    name = ['Mask', 'Dilatation', 'Erosion', 'Opening', 'Closing', 'Gradient', 'Opening & Closing']
    kernel = np.ones(kernel_size, np.uint8)

    dilatation =  cv2.dilate(image, kernel, iterations)
    erode = cv2.erode(image, kernel, iterations)
    opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    gradient = cv2.morphologyEx(image, cv2.MORPH_GRADIENT, kernel)
    open_n_close = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    denoised_images = [mask, dilatation, erode, opening, closing, gradient, open_n_close]

    plt.figure(figsize=(21,7))
    for i in range(len(denoised_images)):
        plt.subplot(1, len(denoised_images), i + 1)
        plt.imshow(denoised_images[i], cmap='gray')
        plt.title(name[i])
    plt.show()