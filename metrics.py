from xmlrpc.client import Boolean
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score, jaccard_score
import numpy as np
import cv2
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

def compute_metrics(prediction, mask, inverse=True, average_method='binary'):
    # Inverse masks
    prediction = (prediction / 255).astype('int')
    if inverse:
        prediction = np.abs(prediction - 1)

    # Compute metrics
    print('Precision: {:.3f}'.format(precision_score(mask.flatten(), prediction.flatten())))
    print('Recall: {:.3f}'.format(recall_score(mask.flatten(), prediction.flatten())))
    print('F1 score: {:.3f}'.format(f1_score(mask.flatten(), prediction.flatten())))
    print('Accuracy: {:.3f}'.format(accuracy_score(mask.flatten(), prediction.flatten())))
    print('Jaccard Score: {:.3f}'.format(jaccard_score(mask.flatten(), prediction.flatten(), average=average_method)))

def visualize_water_mask(mask:np.array, hydro:np.array):
    # Cropping mask to size (w, h)
    cropped_mask = mask[:, :548]
    # Cropping hydro to same size (w, h)
    cropped_hydro = hydro[:1121, :]
    # Modifying the values of the 0 pixels in the hydro image to 255
    cropped_hydro[cropped_hydro == 0] = 255
    superposed_image = cv2.addWeighted(cropped_mask, 0.5, cropped_hydro, 0.5, 0)
    img_list = [cropped_mask, cropped_hydro, superposed_image]
    title_list = ['Mask', 'Hydro', 'Water Mask']
    plt.figure(figsize=(21,7))
    for i in range(len(img_list)):
        plt.subplot(1, len(img_list), i + 1)
        plt.imshow(img_list[i], cmap='gray')
        plt.title(title_list[i])
    plt.show()
    return superposed_image

def color_water_mask(water_mask):
    cmap = ListedColormap(['blue','red','white']) 
    plt.imshow(water_mask, cmap=cmap)
    plt.title('Colored water mask')
    legend_elements = [
        Patch(facecolor='white', edgecolor='white', label='Land'),
        Patch(facecolor='red', edgecolor='red', label='Flooded area'),
        Patch(facecolor='blue', edgecolor='blue', label='Permanent water')
    ]   
    plt.legend(handles=legend_elements, bbox_to_anchor=(1, 1), loc='upper left')
    plt.show()